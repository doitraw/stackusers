package com.example.stackusers.extensions

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

fun View.switchVisibility(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

fun ImageView.loadImage(url: String) =
    Glide.with(this)
        .load(url)
        .apply(RequestOptions.circleCropTransform())
        .into(this)

fun Fragment.loadImageForSharedTransition(
    url: String,
    destination: ImageView
) = Glide.with(this)
    .load(url)
    .centerCrop()
    .apply(RequestOptions.circleCropTransform())
    .dontAnimate()
    .into(destination)

fun EditText.textChangedFlow(defaultEmptyValue: Boolean = false): Flow<String> =
    callbackFlow {
        this@textChangedFlow.apply {
            text.takeIf { it.isNotEmpty() || defaultEmptyValue }?.let {
                offer(it.toString())
            }
            doAfterTextChanged {
                offer(it.toString())
            }
        }
        awaitClose()
    }
