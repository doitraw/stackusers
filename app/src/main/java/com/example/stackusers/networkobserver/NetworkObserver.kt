package com.example.stackusers.networkobserver

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.consumeAsFlow
import timber.log.Timber


interface NetworkObserver {
    val connectionState: Flow<Boolean>
    fun requestNetworkStatusUpdates()
    fun stopNetworkUpdates()
}

class CoroutinesNetworkObserver(
    private val connectivityManager: ConnectivityManager
) : NetworkObserver, ConnectivityManager.NetworkCallback() {
    private val networkRequest: NetworkRequest =
        NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()

    override fun onLost(network: Network) {
        super.onLost(network)
        _connectionStateChannel.offer(false)
    }

    override fun onAvailable(network: Network) {
        super.onAvailable(network)
        _connectionStateChannel.offer(true)
    }

    override fun stopNetworkUpdates() {
        connectivityManager.unregisterNetworkCallback(this)
    }

    private val _connectionStateChannel = BroadcastChannel<Boolean>(Channel.CONFLATED).apply {
        invokeOnClose {
            Timber.d("Removing location updates")
        }
    }
    override val connectionState: Flow<Boolean>
        get() = _connectionStateChannel.openSubscription().consumeAsFlow()

    override fun requestNetworkStatusUpdates() {
        connectivityManager.registerNetworkCallback(networkRequest, this)
    }
}