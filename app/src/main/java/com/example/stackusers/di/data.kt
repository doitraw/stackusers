package com.example.stackusers.di

import com.example.stackusers.data.StackUsersRepository
import com.example.stackusers.data.StackUsersRepositoryImpl
import org.koin.dsl.module

val dataModule = module {
    single<StackUsersRepository> { StackUsersRepositoryImpl(get(),get()) }
}