package com.example.stackusers.di

import android.content.Context
import android.net.ConnectivityManager
import com.example.stackusers.networkobserver.CoroutinesNetworkObserver
import com.example.stackusers.networkobserver.NetworkObserver
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val connectivityModule = module {
    single { androidApplication().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }
    single<NetworkObserver> { CoroutinesNetworkObserver(get()) }
}