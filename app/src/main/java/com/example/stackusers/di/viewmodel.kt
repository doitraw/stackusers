package com.example.stackusers.di

import com.example.stackusers.feature.list.UsersViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { UsersViewModel(get(), get()) }
}