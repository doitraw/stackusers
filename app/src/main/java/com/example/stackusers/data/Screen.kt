package com.example.stackusers.data

import com.example.stackusers.data.presentation.UserItem

sealed class Screen
data class Details(val user: UserItem, val position: Int) : Screen()