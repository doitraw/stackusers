package com.example.stackusers.data

sealed class Result<T> {
    data class Success<T>(val value: T) : Result<T>()
    class Error<Nothing> : Result<Nothing>()
}