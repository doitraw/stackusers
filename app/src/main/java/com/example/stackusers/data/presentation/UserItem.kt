package com.example.stackusers.data.presentation

import android.os.Parcelable
import com.example.stackusers.common.AdapterItem
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class UserItem(
    val id: Int,
    val username: String,
    val url: String,
    val reputation: Int,
    val location: String,
    val createdAt: Date,
    val blocked: Boolean,
    val favourite: Boolean
) : Parcelable, AdapterItem