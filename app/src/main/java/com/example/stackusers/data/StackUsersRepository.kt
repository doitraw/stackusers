package com.example.stackusers.data

import com.example.cache.LocalDataSource
import com.example.networking.RemoteDataSource
import com.example.stackusers.common.AdapterItem
import timber.log.Timber
import java.io.IOException

interface StackUsersRepository {
    suspend fun getUsers(page: Int): Result<List<AdapterItem>>
    suspend fun changeUserState(userId: Int, blocked: Boolean, favourite: Boolean): Result<Unit>
}

class StackUsersRepositoryImpl(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) : StackUsersRepository {

    override suspend fun getUsers(page: Int): Result<List<AdapterItem>> {
        return remoteDataSource.safeCall(
            call = { getUsers(page) },
            mapTo = { users ->
                localDataSource.getBlockedAndFavouriteUsers().let {
                    users.toPresentationModel(it) ?: listOf()
                }
            }
        )
    }

    override suspend fun changeUserState(
        userId: Int,
        blocked: Boolean,
        favourite: Boolean
    ): Result<Unit> = localDataSource.safeCall(
        call = {
            changeUserState(
                userId, blocked, favourite
            )
        },
        mapTo = {}
    )


    private inline fun <T, R> RemoteDataSource.safeCall(
        call: RemoteDataSource.() -> T,
        mapTo: (T) -> R,
        onSuccess: (T) -> Unit = {},
        onError: () -> Unit = {}
    ): Result<R> =
        try {
            Result.Success(mapTo(call().also { onSuccess(it) }))
        } catch (e: IOException) {
            onError()
            Timber.e(e)
            Result.Error()
        }

    private inline fun <T, R> LocalDataSource.safeCall(
        call: LocalDataSource.() -> T,
        mapTo: (T) -> R,
        onSuccess: (T) -> Unit = {},
        onError: () -> Unit = {}
    ): Result<R> =
        try {
            Result.Success(mapTo(call().also { onSuccess(it) }))
        } catch (e: IOException) {
            onError()
            Timber.e(e)
            Result.Error()
        }
}
