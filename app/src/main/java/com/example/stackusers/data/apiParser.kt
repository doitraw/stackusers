package com.example.stackusers.data

import com.example.cache.SelectedUserEntity
import com.example.networking.model.StackUsersResponse
import com.example.stackusers.data.presentation.UserItem
import java.util.*

fun StackUsersResponse.toPresentationModel(selectedUsers: List<SelectedUserEntity>): List<UserItem>? =
    users?.map {user ->
        UserItem(
            id = user.userId ?: -1,
            username = user.displayName.orEmpty(),
            url = user.profileImage.orEmpty(),
            reputation = user.reputation ?: -1,
            location = user.location.orEmpty(),
            createdAt = Date(user.creationDate?.toLong() ?: 0),
            favourite = selectedUsers.any { it.userId == user.userId && it.favourite },
            blocked = selectedUsers.any { it.userId == user.userId && it.blocked }
        )
    }

