package com.example.stackusers.feature.list

import com.example.stackusers.data.presentation.UserItem

sealed class UserAction
object Load : UserAction()
object Retry : UserAction()
object LoadMore : UserAction()
class ShowDetails(val item: UserItem, val position: Int) : UserAction()
class ToggleFavouriteState(val position: Int) : UserAction()
class ToggleBlockedState(val position: Int) : UserAction()