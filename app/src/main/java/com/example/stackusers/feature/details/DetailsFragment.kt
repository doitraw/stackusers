package com.example.stackusers.feature.details

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.transition.TransitionInflater
import com.example.stackusers.R
import com.example.stackusers.common.DefaultTransitionListener
import com.example.stackusers.data.PHOTO
import com.example.stackusers.data.TRANSITION
import com.example.stackusers.data.presentation.UserItem
import com.example.stackusers.extensions.loadImageForSharedTransition
import kotlinx.android.synthetic.main.fragment_details.*
import timber.log.Timber


class DetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_details, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("Saved instance is $savedInstanceState")
        sharedElementEnterTransition =
            TransitionInflater.from(context)
                .inflateTransition(android.R.transition.move)
                .addListener(DefaultTransitionListener {
                    detailsTv.animate().alpha(1.0f)
                })
        sharedElementReturnTransition = TransitionInflater.from(context)
            .inflateTransition(android.R.transition.move)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        savedInstanceState?.let { detailsTv.alpha = 1.0f }
        arguments?.let {
            photoIv.apply {
                transitionName = it.getString(TRANSITION)
                it.getParcelable<UserItem>(PHOTO)?.let {
                    detailsTv.apply {
                        movementMethod = ScrollingMovementMethod()
                        text = context.getString(R.string.description, it.username, it.reputation)
                    }
                    loadImageForSharedTransition(it.url, this)
                }
            }
        }
    }
}