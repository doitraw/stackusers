package com.example.stackusers.feature.list

import androidx.lifecycle.*
import com.example.stackusers.common.AdapterItem
import com.example.stackusers.data.Details
import com.example.stackusers.data.Result
import com.example.stackusers.data.Screen
import com.example.stackusers.data.StackUsersRepository
import com.example.stackusers.data.presentation.PagingItem
import com.example.stackusers.data.presentation.RetryItem
import com.example.stackusers.data.presentation.UserItem
import com.example.stackusers.networkobserver.NetworkObserver
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import timber.log.Timber

class UsersViewModel(
    private val repository: StackUsersRepository,
    private val networkObserver: NetworkObserver
) : ViewModel() {

    private val userActions: MutableLiveData<UserAction> = MutableLiveData()
    private val navigationActions: MutableLiveData<ShowDetails> = MutableLiveData()
    private var userActionsJob: Job? = null

    private var page: Int = 1
    private var list: List<AdapterItem> = emptyList()

    val navigationState: LiveData<Screen> = navigationActions.switchMap { action ->
        liveData<Screen> { emit(Details(action.item, action.position)) }
    }

    val viewState: LiveData<ViewState> = userActions.switchMap { action ->
        liveData {
            when (action) {
                Load -> {
                    emit(ViewState.Loading)
                    emit(fetchMore())
                }
                Retry -> {
                    list = list.toMutableList().apply {
                        removeAll { it is RetryItem }
                        add(PagingItem)
                    }.also { emit(ViewState.Loaded(it)) }
                    emit(fetchMore())
                }
                LoadMore -> emit(fetchMore())
                //TODO: this needs some refactor
                is ToggleFavouriteState -> {
                    action.position.takeUnless { it < 0 }?.let { position ->
                        val item = list[position] as UserItem
                        repository.changeUserState(item.id, !item.favourite, item.blocked)
                            .takeIf { result -> result is Result.Success }
                            ?.let {
                                list = list.toMutableList().apply {
                                    set(
                                        position,
                                        item.copy(favourite = !item.favourite)
                                    )
                                }.also {
                                    emit(ViewState.Loaded(it))
                                }
                            }
                    }
                }
                //TODO: this needs some refactor
                is ToggleBlockedState ->
                    action.position.takeUnless { it < 0 }?.let { position ->
                        val item = list[position] as UserItem
                        repository.changeUserState(item.id, item.favourite, !item.blocked)
                            .takeIf { result -> result is Result.Success }
                            ?.let {
                                list = list.toMutableList().apply {
                                    set(
                                        position,
                                        item.copy(blocked = !item.blocked)
                                    )
                                }.also {
                                    emit(ViewState.Loaded(it))
                                }
                            }
                    }
            }
        }
    }

    init {
        userActions.postValue(Load)
        networkObserver.requestNetworkStatusUpdates()
        networkObserver.connectionState
            .onEach { connected ->
                if (connected && list.contains(RetryItem)) userActions.postValue(Retry)
            }
            .launchIn(viewModelScope)
    }

    fun consumeActions(vararg actions: Flow<UserAction>?) {
        //TODO: find a better way!
        userActionsJob?.cancel()
        userActionsJob = actions.filterNotNull().merge()
            .onEach {
                Timber.d("Ojezu, $it")
                when (it) {
                    is ShowDetails -> navigationActions.postValue(it)
                    else -> userActions.postValue(it)
                }
            }
            .launchIn(viewModelScope)
    }


    private suspend fun fetchMore(): ViewState {
        list = list.toMutableList().apply {
            removeAll { it is PagingItem }
            when (val items = repository.getUsers(page)) {
                is Result.Success -> {
                    page++
                    addAll(items.value)
                    add(PagingItem)
                }
                is Result.Error -> {
                    add(RetryItem)
                }
            }
        }
        return ViewState.Loaded(list)
    }

    override fun onCleared() {
        networkObserver.stopNetworkUpdates()
        super.onCleared()
    }
}