package com.example.stackusers.feature.list

import com.example.stackusers.common.AdapterItem

sealed class ViewState {
    object Loading : ViewState()
    data class Loaded(val items: List<AdapterItem>) : ViewState()
}

