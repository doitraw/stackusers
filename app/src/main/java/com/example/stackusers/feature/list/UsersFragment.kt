package com.example.stackusers.feature.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.os.bundleOf
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.stackusers.R
import com.example.stackusers.common.UsersAdapter
import com.example.stackusers.data.Details
import com.example.stackusers.data.PHOTO
import com.example.stackusers.data.TRANSITION
import com.example.stackusers.data.presentation.UserItem
import com.example.stackusers.extensions.switchVisibility
import kotlinx.android.synthetic.main.fragment_gallery.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class UsersFragment : Fragment() {

    private val viewModel: UsersViewModel by viewModel()

    private val usersAdapter = UsersAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_gallery, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postponeEnterTransition()
        dataRv.apply {
            adapter = usersAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            doOnPreDraw {
                startPostponedEnterTransition()
                animate().alpha(1.0f)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.apply {
            viewState.observe(viewLifecycleOwner, Observer { renderViewState(it) })
            navigationState.observe(viewLifecycleOwner, Observer {
                when (it) {
                    is Details -> goToDetails(it.user, it.position)
                }
            })
            if (savedInstanceState == null) consumeActions (usersAdapter.actions)
        }
    }

    private fun goToDetails(userItem: UserItem, position: Int) {
        dataRv.layoutManager?.findViewByPosition(position)?.findViewWithTag<ImageView>(userItem.id)
            ?.let {
                findNavController().navigate(
                    R.id.detailsFragment,
                    bundleOf(PHOTO to userItem, TRANSITION to it.transitionName),
                    null,
                    FragmentNavigatorExtras(it to it.transitionName)
                )
            }
    }

    private fun renderViewState(state: ViewState) {
        progressBar.switchVisibility(state == ViewState.Loading)
        if (state is ViewState.Loaded) usersAdapter.list = state.items
    }
}
