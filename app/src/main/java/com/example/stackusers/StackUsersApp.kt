package com.example.stackusers

import android.app.Application
import com.example.cache.cacheModule
import com.example.networking.networkingModule
import com.example.stackusers.di.connectivityModule
import com.example.stackusers.di.dataModule
import com.example.stackusers.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class StackUsersApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@StackUsersApp)
            Timber.plant(Timber.DebugTree())
            modules(
                listOf(
                    cacheModule,
                    connectivityModule,
                    dataModule,
                    viewModelModule,
                    networkingModule
                )
            )
        }
    }
}