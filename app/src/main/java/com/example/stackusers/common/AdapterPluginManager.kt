package com.example.stackusers.common

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListUpdateCallback
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.flow.Flow
import timber.log.Timber
import kotlin.properties.Delegates

abstract class AdapterPluginManager<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    open val actions: Flow<T>? = null

    var list: List<AdapterItem> by Delegates.observable(listOf()) { _, oldValue, newValue ->
        DiffUtil.calculateDiff(AdapterItemDiffCallback(oldValue, newValue))
            .dispatchUpdatesTo(AdapterListUpdateCallback())
    }

    protected val plugins: MutableList<AdapterPlugin<T>> = mutableListOf()

    protected fun addPlugin(plugin: AdapterPlugin<T>) {
        plugins.add(plugin)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        plugins[getItemViewType(position)].onBindViewHolder(list[position], holder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        plugins[viewType].onCreateViewHolder(parent)

    override fun getItemViewType(position: Int): Int =
        plugins.indexOfFirst { it.isForViewType(list[position]) }

    override fun getItemCount(): Int = list.size

    inner class AdapterItemDiffCallback(
        private val oldList: List<AdapterItem>,
        private val newList: List<AdapterItem>
    ) :
        DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition] === newList[newItemPosition]

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]

        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size
    }

    inner class AdapterListUpdateCallback : ListUpdateCallback {
        override fun onChanged(position: Int, count: Int, payload: Any?) {
            notifyItemChanged(position)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {}

        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }
    }
}