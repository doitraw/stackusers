package com.example.stackusers.common

import androidx.transition.Transition

class DefaultTransitionListener(val onEnd: () -> Unit) : Transition.TransitionListener {
    override fun onTransitionEnd(transition: Transition) {
        onEnd()
    }

    override fun onTransitionResume(transition: Transition) {
    }

    override fun onTransitionPause(transition: Transition) {
    }

    override fun onTransitionCancel(transition: Transition) {
    }

    override fun onTransitionStart(transition: Transition) {
    }
}