package com.example.stackusers.common

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow

abstract class AdapterPlugin<T> {
    protected open val actionsChannel: BroadcastChannel<T>? = null
    val actions: Flow<T>?
        get() = actionsChannel?.asFlow()

    abstract fun isForViewType(item: AdapterItem): Boolean
    abstract fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
    abstract fun onBindViewHolder(item: AdapterItem, viewHolder: RecyclerView.ViewHolder)
}