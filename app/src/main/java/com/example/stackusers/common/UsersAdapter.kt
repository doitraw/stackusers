package com.example.stackusers.common

import com.example.stackusers.common.plugins.PagingPlugin
import com.example.stackusers.common.plugins.RetryPlugin
import com.example.stackusers.common.plugins.UsersPlugin
import com.example.stackusers.feature.list.UserAction
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.merge

class UsersAdapter : AdapterPluginManager<UserAction>() {
    init {
        addPlugin(UsersPlugin())
        addPlugin(PagingPlugin())
        addPlugin(RetryPlugin())
    }

    override val actions: Flow<UserAction>?
        get() = plugins.mapNotNull { it.actions }.merge()
}