package com.example.stackusers.common.plugins

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.stackusers.R
import com.example.stackusers.common.AdapterItem
import com.example.stackusers.common.AdapterPlugin
import com.example.stackusers.data.presentation.RetryItem
import com.example.stackusers.feature.list.Retry
import com.example.stackusers.feature.list.UserAction
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel

class RetryPlugin : AdapterPlugin<UserAction>() {

    override val actionsChannel: BroadcastChannel<UserAction> = BroadcastChannel(Channel.BUFFERED)

    override fun onCreateViewHolder(parent: ViewGroup) = Holder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_retry,
            parent,
            false
        )
    )

    override fun onBindViewHolder(item: AdapterItem, viewHolder: RecyclerView.ViewHolder) =
        (viewHolder as Holder).bind()

    override fun isForViewType(item: AdapterItem): Boolean = item is RetryItem

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            itemView.setOnClickListener { actionsChannel.offer(Retry) }
        }
    }
}