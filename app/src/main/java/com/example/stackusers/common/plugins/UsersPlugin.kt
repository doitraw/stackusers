package com.example.stackusers.common.plugins

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.stackusers.R
import com.example.stackusers.common.AdapterItem
import com.example.stackusers.common.AdapterPlugin
import com.example.stackusers.data.presentation.UserItem
import com.example.stackusers.extensions.loadImage
import com.example.stackusers.feature.list.ShowDetails
import com.example.stackusers.feature.list.ToggleBlockedState
import com.example.stackusers.feature.list.ToggleFavouriteState
import com.example.stackusers.feature.list.UserAction
import kotlinx.android.synthetic.main.item_user.view.*
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel

class UsersPlugin : AdapterPlugin<UserAction>() {

    override val actionsChannel: BroadcastChannel<UserAction> = BroadcastChannel(Channel.BUFFERED)

    override fun onCreateViewHolder(parent: ViewGroup) = Holder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_user,
            parent,
            false
        )
    )

    override fun onBindViewHolder(item: AdapterItem, viewHolder: RecyclerView.ViewHolder) =
        (viewHolder as Holder).bind(item as UserItem)

    override fun isForViewType(item: AdapterItem): Boolean = item is UserItem

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: UserItem) {
            itemView.apply {
                setOnClickListener { actionsChannel.offer(ShowDetails(item, adapterPosition)) }
                favouriteBtn.setOnClickListener {
                    actionsChannel.offer(
                        ToggleFavouriteState(
                            adapterPosition
                        )
                    )
                }
                blockBtn.setOnClickListener {
                    actionsChannel.offer(
                        ToggleBlockedState(
                            adapterPosition
                        )
                    )
                }
                photoIv.apply {
                    transitionName = item.id.toString()
                    tag = item.id
                    loadImage(item.url)
                }
                nameTv.text = item.username
                //TODO: temporary, need to add payloads
                favouriteBtn.setImageResource(if (item.favourite) R.drawable.ic_star_black_24dp else R.drawable.ic_star_border_black_24dp)
                setBackgroundColor(resources.getColor(if (item.blocked) R.color.grey else R.color.white))
            }
        }
    }
}