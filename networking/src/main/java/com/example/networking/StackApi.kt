package com.example.networking

import com.example.networking.model.StackUsersResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface StackApi {

    @GET("users")
    suspend fun getUsers(
        @Query("page") page: Int = 1,
        @Query("pagesize") pageSize: Int = 20,
        @Query("order") order: String = "desc",
        @Query("sort") sort: String = "reputation",
        @Query("site") site: String = "stackoverflow"
    ): Response<StackUsersResponse>
}