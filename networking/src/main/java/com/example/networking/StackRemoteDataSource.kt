package com.example.networking

import com.example.networking.model.StackUsersResponse
import java.io.IOException

interface RemoteDataSource {
    @Throws(IOException::class)
    suspend fun getUsers(page:Int): StackUsersResponse
}

class StackRemoteDataSource(
    private val stackApi: StackApi
) : RemoteDataSource {

    override suspend fun getUsers(page: Int) =
        stackApi.getUsers(page).takeIf { it.isSuccessful }?.body() ?: throw IOException()
}