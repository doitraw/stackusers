package com.example.networking

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkingModule = module {

    single<OkHttpClient> {
        OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(get())
            .build()
    }

    single<Interceptor> {
        HttpLoggingInterceptor().apply {
            level =
                HttpLoggingInterceptor.Level.BODY.takeIf { BuildConfig.DEBUG }
                    ?: HttpLoggingInterceptor.Level.NONE
        }
    }

    single<StackApi> {
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(get())
            .build()
            .create(StackApi::class.java)
    }

    single<RemoteDataSource> { StackRemoteDataSource(get()) }
}

