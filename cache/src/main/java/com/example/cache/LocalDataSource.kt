package com.example.cache

interface LocalDataSource {
    suspend fun getBlockedAndFavouriteUsers(): List<SelectedUserEntity>
    suspend fun changeUserState(userId: Int, blocked: Boolean, favourite: Boolean)
}

class SelectedUsersDataSource(private val selectedUsersDao: SelectedUsersDao) : LocalDataSource {
    override suspend fun changeUserState(userId: Int, blocked: Boolean, favourite: Boolean) {
        selectedUsersDao.updateSelectedUser(SelectedUserEntity(userId, favourite, blocked))
    }

    override suspend fun getBlockedAndFavouriteUsers(): List<SelectedUserEntity> =
        selectedUsersDao.getUsers()
}