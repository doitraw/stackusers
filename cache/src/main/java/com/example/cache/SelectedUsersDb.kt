package com.example.cache

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(version = 1, entities = [SelectedUserEntity::class])
abstract class SelectedUsersDb : RoomDatabase() {

    abstract fun selectedUsersDao(): SelectedUsersDao

}