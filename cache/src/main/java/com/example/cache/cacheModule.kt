package com.example.cache

import androidx.room.Room
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val cacheModule = module {

    single {
        Room.databaseBuilder(androidContext(), SelectedUsersDb::class.java, "app-database")
            .fallbackToDestructiveMigration()
            .build()
    }
    single { get<SelectedUsersDb>().selectedUsersDao() }

    single<LocalDataSource> {
        SelectedUsersDataSource(
            selectedUsersDao = get()
        )
    }

}
