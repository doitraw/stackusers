package com.example.cache

import androidx.room.*

@Entity(tableName = "SelectedUsers")
data class SelectedUserEntity(
    @PrimaryKey(autoGenerate = false) val userId: Int,
    val favourite: Boolean,
    val blocked: Boolean
)

@Dao
interface SelectedUsersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateSelectedUser(userEntity: SelectedUserEntity)

    @Query("SELECT * FROM SelectedUsers")
    suspend fun getUsers(): List<SelectedUserEntity>
}
